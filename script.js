const get_element = (name) => document.getElementById(name) // For shorter syntax

let bill = { "form": get_element("bill_total"), "error": get_element("bill_error"), "value": NaN }
let tip = { "form": get_element("tip_input"), "error": get_element("tip_error"), "value": 0 }
let people = { "form": get_element("people_input"), "error": get_element("people_error"), value: 0 }

const total_form = document.getElementById("total_form") // Holds the form of the final value

people.value = Number(people.form.innerText) // Initialize the value for the number of people

const get_bill = () => {
  bill.value = Number(bill.form.value) // Get the value from the form
  
  // Error, Non-accepted character was entered
  if (Number.isNaN(bill.value)) { set_bill_error("The value contains non-digit characters") }
    
  // Error, The value for the bill cannot be 0
  else if (bill.value === 0) { set_bill_error("0 is not an accepted bill amount") }
    
  else { // Accepted number
    // JS will format a number that starts with 0 like "072". Check against that
    if (bill.form.value[0] === "0") { set_bill_error("The number cannot start with 0") }
    else { 
      bill.error.style.display = "none"; // Clear the error text if it exists for a previous wrong value

      // If an accepted tip was given we could still had an error in case there was no
      // bill value first so after we get the bill, we need to call this function to check
      get_tip()
    }
  }

  calculate_final_result()
}

const get_tip = () => {
  if (!Number.isNaN(bill.value)) { // A bill amount is required before the tip percentage
    tip.value = Number(tip.form.value)

    // Check if the value is a valid number but in this case, we accept 0
    if (Number.isNaN(tip.value)) { set_tip_error("The value contains non-digit characters") }
    else                         { tip.error.style.display = "none"; }
  }

  else { set_tip_error("Bill amount was not given") }

  calculate_final_result()
}

const increasePeople = () => {
  people.value += 1 // Increment the number of people
  people.form.innerText = people.value // update the DOM with the new number of people
  calculate_final_result()
}

const decreasePeople = () => {
  if (people.value > 1) { people.value -= 1} // If it is possible, decremnet the number of people
  // else                  { set_people_error() } // If not, show the error

  people.form.innerText = people.value
  calculate_final_result()
}

const calculate_final_result = () => {
  // Check if values for both "bill" and "tip" were properly given
  if (Number.isNaN(bill.value) || Number.isNaN(tip.value)) { total_form.innerText = "0 €" }
  else {
    // Calculate the final total per person
    let tip_amount = bill.value * (tip.value / 100)
    let final_result = (bill.value + tip_amount) / people.value
    // In case the number is something like "37.3333333337", trim it and keep only the first 2 digits
    if (!Number.isInteger(final_result)) { final_result = final_result.toFixed(2) }
    total_form.innerText = String(final_result) + " €" // We need to explecitely add the currency here
  }
}


const set_bill_error = (msg) => {
  bill.error.innerText = msg
  bill.error.style.display = "block"
  bill.error.style.color = "#ef233c"
  bill.error.style.fontStyle = "italic"
  bill.value = NaN
}

const set_tip_error = (msg) => {
  tip.error.innerText = msg
  tip.error.style.display = "block"
  tip.error.style.color = "#ef233c"
  tip.error.style.fontStyle = "italic"
  tip.value = NaN
}

// TODO: For future, ignore
// const set_people_error = () => {
//   var intervalID = 0;
//   console.log(intervalID)

  
//   let fade_out_fn = () => {
// 		opacity = Number(window.getComputedStyle(people.error).getPropertyValue("opacity"));
//     console.log(opacity) // NOTE: This keeps running and running...
// 		if (opacity > 0) {
// 			opacity = opacity - 0.1
// 			people.error.style.opacity = opacity
// 		}
      
// 		else { clearInterval(intervalID) }
//   }

//   // Set the text
//   people.error.innerText = "The number of people cannot be less than 1"
//   people.error.style.display = "block"
//   people.error.style.color = "#ef233c"
//   people.error.style.fontStyle = "italic"
  
//   setInterval(fade_out_fn, 100);
// }